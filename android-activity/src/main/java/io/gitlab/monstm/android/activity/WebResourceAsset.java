package io.gitlab.monstm.android.activity;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebResourceResponse;

import java.nio.charset.StandardCharsets;

public class WebResourceAsset extends WebResourceResponse {
	public WebResourceAsset(Context context, Uri uri){
		super("application/octet-stream", "UTF-8", null);

		String filename = uri.getHost() + uri.getPath();

		try{
			setMimeType(ApacheMimeType.get(filename));
			setEncoding(StandardCharsets.UTF_8.toString());
			setData(context.getAssets().open(filename));
		}catch(Exception e){
			Log.w("WebResourceAsset", uri.getScheme() + "://" + filename, e);

			setStatusCodeAndReasonPhrase(404, "Not Found");
		}
	}
}
