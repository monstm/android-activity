package io.gitlab.monstm.android.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.webkit.*;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;

abstract public class WebActivity extends AppCompatActivity {
	private LinearLayout layout;
	private WebView webview;


	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

//		getActionBar().hide();
		getSupportActionBar().hide();

		ApacheMimeType.Init(this, "application/octet-stream");

		initWebView();
		initLayout();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView(){
		webview = new WebView(this.getBaseContext());

		webview.setWebViewClient(new WebViewClient(){
			@Override
			public void onReceivedSslError(WebView v, SslErrorHandler handler, SslError er){
				handler.proceed();
			}

			@Override
			public WebResourceResponse shouldInterceptRequest(final WebView view, WebResourceRequest request){
				WebResourceResponse ret;
//
//				Uri uri = request.getUrl();
//
//				switch(uri.getScheme().toLowerCase()){
//					case "asset":
//						ret = new WebResourceAsset(view.getContext(), uri);
//						break;
//					default:
						ret = super.shouldInterceptRequest(view, request);
//						break;
//				}

//				Log.d("WebResourceResponse(" + uri.getScheme() + "://" + uri.getHost() + uri.getPath() + ")", ret.getMimeType());

				return ret;
			}
		});

		webview.setWebChromeClient(new WebChromeClient());

		WebSettings websettings = webview.getSettings();
		websettings.setJavaScriptEnabled(true);
		websettings.setAllowFileAccess(true);
		websettings.setAllowFileAccessFromFileURLs(true);
		websettings.setAllowUniversalAccessFromFileURLs(true);
	}

	private void initLayout(){
		layout = new LinearLayout(this);

		LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT
		);

		layout.setLayoutParams(layoutparams);

		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.addView(webview);

		setContentView(layout);
	}

	public void loadUrl(String Url){
		webview.loadUrl(Url);
	}
}
