package io.gitlab.monstm.android.activity;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class ApacheMimeType {
	private static HashMap<String, String> hm = new HashMap<String, String>();
	private static String default_mime_type = "";

	public static void Init(Context context, String DefaultMimeType){
		BufferedReader br;

		String readLine;
		String[] line_split;
		String[] extension;
		String mimetype;

		int index, count;

		try {
			br = new BufferedReader(
					new InputStreamReader(
							context.getAssets().open("mime.types"),
							StandardCharsets.UTF_8
					)
			);

			while((readLine = br.readLine()) != null){
				readLine = readLine.trim().replace("\t", " ");

				if(readLine.charAt(0) != '#'){
					line_split = readLine.split(" ", 2);
					if(line_split.length == 2){
						mimetype = line_split[0].trim();

						if(!mimetype.equals("")){
							extension = line_split[1].trim().split(" ");
							count = extension.length;
							for(index = 0; index < count; index++){
								hm.put(extension[index], mimetype);
							}
						}
					}
				}
			}

			br.close();
		}catch(IOException e){
			e.printStackTrace();
		}

		default_mime_type = DefaultMimeType;
	}

	public static String get(String filename){
		String
				ret,
				extension = filename.substring(filename.lastIndexOf(".")+1);

		try{
			ret = hm.get(extension);
		}catch(Exception e){
			Log.w("ApacheMimeType(" + extension + ")", filename, e);

			ret = default_mime_type;
		}
		return ret;
	}
}
