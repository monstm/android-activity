window.addEventListener('DOMContentLoaded', function(){
	window.FIREBASE_AUTH.onAuthStateChanged(function(user){
		[
			{ 'selector': '.user-name', 'key': '/' + user.uid + '/name', 'default': '' },
			{ 'selector': '.user-birthdate', 'key': '/' + user.uid + '/birthdate', 'default': '1970-01-01' },
			{ 'selector': '.user-weight', 'key': '/' + user.uid + '/weight', 'default': '' },
			{ 'selector': '.user-address', 'key': '/' + user.uid + '/address', 'default': '' },
			{ 'selector': '.user-phone', 'key': '/' + user.uid + '/phone', 'default': '' },

			{ 'selector': '.exam-time', 'key': '/' + user.uid + '/exam-time', 'default': '-' },
			{ 'selector': '.exam-risk', 'key': '/' + user.uid + '/exam-risk', 'default': '-' },
			{ 'selector': '.exam-recommendation', 'key': '/' + user.uid + '/exam-recommendation', 'default': '-' }
		].forEach(function(Data){
			window.FIREBASE_DATAGET(Data.key, Data.default, function(Result){
				document
					.querySelectorAll(Data.selector)
					.forEach(function(Element){
						if(Element instanceof HTMLInputElement){
							Element.value = Result;
						}else{
							Element.innerHTML = Result;
						}
					});
			});
		});
	});
});