import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';
import { getDatabase, ref, set, child, get } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-database.js';
import { getAnalytics } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-analytics.js';


window.FIREBASE_APP = initializeApp({
	'apiKey': 'AIzaSyBGVU8ICq9iEl6T6gjboYzQzN6jwIrBEew',
	'authDomain': 'fluted-bit-325907.firebaseapp.com',
	'projectId': 'fluted-bit-325907',
	'databaseURL': 'https://fluted-bit-325907-default-rtdb.asia-southeast1.firebasedatabase.app',
	'storageBucket': 'fluted-bit-325907.appspot.com',
	'messagingSenderId': '389029342377',
	'appId': '1:389029342377:web:154fad8bc6c59dce4fcdb8',
	'measurementId': 'G-9F74QQP8RX'
});


window.FIREBASE_AUTH = getAuth(window.FIREBASE_APP);

window.FIREBASE_AUTH.onAuthStateChanged(function(user){
	window.FIREBASE_USER_ID = (user ? user.uid : null);
});

window.FIREBASE_SIGNUP = function(Email, Password, SuccessCallback, ErrorCallback){
	createUserWithEmailAndPassword(window.FIREBASE_AUTH, Email, Password)
		.then((userCredential) => {
			if(typeof(SuccessCallback) == 'function'){
				SuccessCallback(userCredential);
			}
		})
		.catch((error) => {
			if(typeof(ErrorCallback) == 'function'){
				ErrorCallback(error);
			}
		});
};

window.FIREBASE_SIGNIN = function(Email, Password, SuccessCallback, ErrorCallback){
	signInWithEmailAndPassword(window.FIREBASE_AUTH, Email, Password)
		.then((userCredential) => {
			if(typeof(SuccessCallback) == 'function'){
				SuccessCallback(userCredential);
			}
		})
		.catch((error) => {
			if(typeof(ErrorCallback) == 'function'){
				ErrorCallback(error);
			}
		});
};

window.FIREBASE_SIGNOUT = function(SuccessCallback, ErrorCallback){
	signOut(window.FIREBASE_AUTH)
		.then(() => {
			if(typeof(SuccessCallback) == 'function'){
				SuccessCallback();
			}
		})
		.catch((error) => {
			if(typeof(ErrorCallback) == 'function'){
				ErrorCallback(error);
			}
		});
};


window.FIREBASE_DATABASE = getDatabase(window.FIREBASE_APP, 'https://fluted-bit-325907-default-rtdb.asia-southeast1.firebasedatabase.app/');

window.FIREBASE_DATASET = function(Key, Value){
	set(ref(window.FIREBASE_DATABASE, Key), Value);
};

window.FIREBASE_DATAGET = function(Key, DefaultValue, ResultCallback){
	if(typeof(ResultCallback) == 'function'){
		get(child(ref(window.FIREBASE_DATABASE), Key))
			.then((snapshot) => {
				if(snapshot.exists()){
					ResultCallback(snapshot.val());
				}else{
					console.warn('FIREBASE_DATAGET(' + Key + '): No data available');
					ResultCallback(DefaultValue);
				}
			})
			.catch((error) => {
				console.error('FIREBASE_DATAGET(' + Key + '): ' + error.message);
				ResultCallback(DefaultValue);
			});
	}
};


window.FIREBASE_ANALYTICS = getAnalytics(window.FIREBASE_APP);
