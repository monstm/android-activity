package samy.android.activity;

import android.os.Bundle;
import io.gitlab.monstm.android.activity.WebActivity;

public class MainActivity extends WebActivity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		loadUrl("https://google.com/");
		loadUrl("file:///android_asset/index.html");
//		loadUrl("asset://index.html");
	}
}
